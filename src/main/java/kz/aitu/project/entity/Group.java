package kz.aitu.project.entity;


import lombok.Data;

@Data
public class Group {
    private int groupId;
    private String groupName;

    @Override
    public String toString() {
        return "Group Id: " + groupId + "   Group name: " + groupName;
    }
}
