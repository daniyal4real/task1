package kz.aitu.project.entity;


import lombok.Data;

@Data
public class Student {
    private int studentId;
    private String studentName;
    private String studentPhone;
    private int groupId;

    @Override
    public String toString() {
        return "Group id: " + groupId + "   Student id: " + studentId + "   Name: " + studentName +
                "   Phone: " + studentPhone ;
    }
}
