package kz.aitu.project.repository;

import kz.aitu.project.entity.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentRepository {
    public Connection getConnection() throws SQLException {
        Connection conn = null;
        String url = "jdbc:postgresql://localhost/task1";
        String user = "postgres";
        String pass = "sysdba";
        try {
            conn = DriverManager.getConnection(url, user, pass);
            System.out.println("Connected!");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return conn;
    }

    public List<Student> getStudents() throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        Connection conn = getConnection();
        String sql = "SELECT * FROM student ORDER BY groupId";
        List<Student> data = new ArrayList<Student>();
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.execute();
            rs = pstmt.getResultSet();

            while(rs.next()) {
                Student student = new Student();
                student.setStudentId(rs.getInt(1));
                student.setStudentName(rs.getString(2));
                student.setStudentPhone(rs.getString(3));
                student.setGroupId(rs.getInt(4));
                data.add(student);
                System.out.println(student.toString());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return data;
    }
}
