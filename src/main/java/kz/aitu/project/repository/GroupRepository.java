package kz.aitu.project.repository;

import kz.aitu.project.entity.Group;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GroupRepository {
    public Connection getConnection() throws SQLException {
        Connection conn = null;
        String url = "jdbc:postgresql://localhost/task1";
        String user = "postgres";
        String pass = "sysdba";
        try {
            conn = DriverManager.getConnection(url, user, pass);
            System.out.println("Connected!");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return conn;
    }

    public List<Group> getGroups() throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        Connection conn = getConnection();
        String sql = "SELECT * FROM groupp ORDER BY groupId";
        List<Group> data = new ArrayList<Group>();
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.execute();
            rs = pstmt.getResultSet();
            while(rs.next()) {
                Group group = new Group();
                group.setGroupId(rs.getInt(1));
                group.setGroupName(rs.getString(2));
                data.add(group);
                System.out.println(group.toString());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return data;
    }
}
