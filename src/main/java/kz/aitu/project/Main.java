package kz.aitu.project;


import kz.aitu.project.repository.GroupRepository;
import kz.aitu.project.repository.StudentRepository;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        GroupRepository groupRepository = new GroupRepository();
        groupRepository.getGroups();

        StudentRepository studentRepository = new StudentRepository();
        studentRepository.getStudents();
    }
}
